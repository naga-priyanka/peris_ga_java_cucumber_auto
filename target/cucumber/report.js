$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("Dan_Client/DataStudio.feature");
formatter.feature({
  "line": 3,
  "name": "Click on refresh for every 30 seconds",
  "description": "\r\nAs a Data collector\r\nI want to click on the refresh icon each 30 seconds\r\nso that i receive an updated data each time i extract the report from Data studio",
  "id": "click-on-refresh-for-every-30-seconds",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@solo"
    }
  ]
});
